public class StaticPoly {

    //Polymorphism:
    //Static Polymorphism
    //To achieve this we can have multiple methods with same name but changes forms based on the number of arguments or the type of arguments.
    public int addition(int a, int b){
        return a+b;
    }

    //overload by changing the number of arguments
    public int addition(int a, int b, int c){
        return a+b+c;
    }

    //overloading by changing the type of the argument
    public double addition(double a,double b){
        return a+b;
    }
}
