public class Dog extends Animal {
    //extends keyword allows us to have this class inherit the attributes and methods of the Animal class.
    //There is no multiple inheritance in Java, what we can do instead is

    private String dogBreed;

    //We use the super() keyword to refer the immediate parent class of a subclass.
    public Dog(){
        super();//Access to the Animal() constructor
        this.dogBreed = "Chihuahua";//If we create an instance of the dog class without parameters, the instance should have access to the properties and methods of the Animal class as well as a default value to the dog breed property of the dof subclass.

    }
    public Dog(String name, String color, String breed){
        super(name,color);
        this.dogBreed = breed;
    }

    public String getDogBreed() {
        return dogBreed;
    }

    public void setDogBreed(String dogBreed) {
        this.dogBreed = dogBreed;
    }

    public void greet(){
        super.call();//Run call() inherited from Animal class.
        System.out.println("Bark!");
    }
}
