public class Computer implements Actions{

    public void sleep() {
        System.out.println("Windows is hibernating");
    }

    public void run() {
        System.out.println("Windows is running.");
    }
}
