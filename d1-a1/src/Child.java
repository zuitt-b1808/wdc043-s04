public class Child extends Parent{

    //annotations can change behaviors of methods.
    public void speak() {
        System.out.println("I am the child!");
    }
}
