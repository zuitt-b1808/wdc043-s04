public class Driver {

    private String name;
    private int age;
    private String address;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public Driver(){

    }

    public Driver(String name, String address, int age) {
        this.name = name;
        this.address = address;
        this.age = age;
    }
    /*
    * Classes have relationships:
    *
    * A Car could have a Driver. - "has relationship" - Composition - allows modelling objects to be made up of other objects.
    * */
}
