public class Animal {
    private String name;
    private String color;

    public void setAnimalName(String name) {
        this.name = name;
    }
    public void setAnimalColor(String color) {
        this.color = color;
    }

    public String getAnimalName() {
        return name;
    }

    public String getAnimalColor() {
        return color;
    }

    public Animal(){
    }

    public Animal(String name,String color) {
        this.name = name;
        this.color = color;
    }

    public void call(){
        System.out.println("Hi my name is " + this.name);
    }

}

