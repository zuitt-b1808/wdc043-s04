public interface Actions {

    //Interface are used to achieve abstraction. users of our app will only be able to use the methods but not be able to see how

    public void sleep();
    public  void run();
}
