public class Parent {

    //Dynamic Method Dispatch or Runtime polymorphism, the ability of a subclass to inherit a method from a parent but override and change its definition in the subclass.
    public void speak(){
        System.out.println("I am the parent");
    }
}
