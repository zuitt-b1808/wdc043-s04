public class Car {

    //An object is an idea of a real world object.
    //An instance is a tangible copy of an idea instantiated or created from a class.
    //Attributes and Methods
    //public - the variable in the class is accessible in application.
    //private - limit the access and ability
    //setters are public methods which will allow us set the value of the attribute of an instance.
    //getters are public methods which will allow us to get the value of an instance
    private String make;
    private String brand;
    private int price;
    //Methods are functions of an object which allows us to perform certain tasks
    public void start(){
        System.out.println("Vroom! Vroom!");
    }

    //parameters in Java needs its dataType declared
//    public void setMake(String makeParams){
//        //this keyword refers to the object where the constructor or setter is.
//        this.make = makeParams;
//    }
//
//    //make getter
//    public String getMake(){
//        return this.make;
//    }
//
//    public String getBrand(){
//        return this.brand;
//    }

//    public void setMake(String make) {
//        this.make = make;
//    }
//
//    public void setBrand(String brand) {
//        this.brand = brand;
//    }
//
//    public void setPrice(int price) {
//        this.price = price;
//    }

    //To make a property read-only, don't include a setter function
//    public void setBrand(String makeParams){
//        this.brand = makeParams;
//    }

//    public String getMake() {
//        return make;
//    }
//
//    public String getBrand() {
//        return brand;
//    }
//
//    public int getPrice() {
//        return price;
//    }
//

    public void setMake(String make) {
        this.make = make;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getMake() {
        return make;
    }

    public String getBrand() {
        return brand;
    }

    public int getPrice() {
        return price;
    }

    //Add a driver variable to add a property which is an object/instance of a Driver class.
    private Driver carDriver;

    //Methods are functions of an object which allows us to perform certain tasks.

//    public int getPrice(){
//        return this.price;
//    }
//    public void setPrice(int makeParams){
//        this.price = makeParams;
//    }
    /*
    * Create setters and getter for the brand and price property of our object.
    * Update the brand and price of car1 in the Main class using the setter and getters.
    * Print the brand and price of car1 in the Main class using getters.*/

    //contructor is a method which allows us to set the initial values of an instance.
    //empty/default constructor - default constructor - allows us to create an instance with default initialized values for our properties.
    public Car(){
        //empty or you can designate default values instead of getting them from parameters.
        //Java, actually already creates one for us, however, empty/default constructor made just by Java allows us to Java to set its own default values. IF you create your own, you can make your own default values.
    }

//    public Car(String make,String brand, int price){
//        this.make = make;
//        this.brand = brand;
//        this.price = price;
//    }

    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    public Driver getCarDriver() {
        return carDriver;
    }

    public  String getCarDriverName(){
        return this.carDriver.getName();
    }
}
